require('vmark.base')
require('vmark.maps')
require('vmark.plugins')
require('vmark.theme')
require('vmark.cmp')
require('vmark.treesitter')
require('vmark.telescope')
require('vmark.renamer')
require('vmark.git-conflct')
require('vmark.lsp')

local has = function(x)
  return vim.fn.has(x) == 1
end

local is_mac = has "macunix"
local is_win = has "win32"

if is_mac then
  require('vmark.macos')
end
if is_win then
  require('vmark.windows')
end
