local fn = vim.fn

-- Automatically install packer
local install_path = fn.stdpath "data" .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
  PACKER_BOOTSTRAP = fn.system {
    "git",
    "clone",
    "--depth",
    "1",
    "https://github.com/wbthomason/packer.nvim",
    install_path,
  }
  print "Installing packer close and reopen Neovim..."
  vim.cmd [[packadd packer.nvim]]
end

-- Autocommand that reloads neovim whenever you save the plugins.lua file
vim.cmd [[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]]

local status, packer = pcall(require, 'packer')
if (not status) then
  print("Packer is not installed")
  return
end

packer.init {
  display = {
    open_fn = function()
      return require("packer.util").float { border = "rounded" }
    end,
  },
}


packer.startup(function(use)
  use 'wbthomason/packer.nvim'

  use 'williamboman/mason.nvim'
  use 'williamboman/mason-lspconfig.nvim'

  use 'onsails/lspkind-nvim' -- vscode-like pictograms
  use 'L3MON4D3/LuaSnip'
  use 'saadparwaiz1/cmp_luasnip'
  use 'hrsh7th/cmp-buffer'   -- nvim-cmp source for buffer words
  use 'hrsh7th/cmp-nvim-lsp' -- nvim-cmp source for neovim's built-in LSP
  use 'hrsh7th/nvim-cmp'     -- Completion
  use "lukas-reineke/lsp-format.nvim"
  use 'pearofducks/ansible-vim'
  -- use 'glepnir/lspsaga.nvim' -- LSP UIs

  --use {
  --  'svrana/neosolarized.nvim',
  --  requires = { 'tjdevries/colorbuddy.nvim' }
  -- }
  use { "ellisonleao/gruvbox.nvim" }
  -- use "savq/melange"
  -- use "EdenEast/nightfox.nvim"

  use {
    'nvim-lualine/lualine.nvim',
    requires = { 'kyazdani42/nvim-web-devicons', opt = true }
  } -- Statusline

  use { 'neovim/nvim-lspconfig', requires = { 'tjdevries/colorbuddy.nvim' } }

  use {
    'nvim-treesitter/nvim-treesitter',
    run = ':TSUpdate'
  }
  use 'windwp/nvim-autopairs'
  use 'windwp/nvim-ts-autotag'

  use 'nvim-lua/plenary.nvim'
  use 'nvim-telescope/telescope.nvim'
  use 'nvim-telescope/telescope-file-browser.nvim'
  use 'kyazdani42/nvim-web-devicons' -- File icons

  use 'akinsho/nvim-bufferline.lua'

  use('jose-elias-alvarez/null-ls.nvim')
  use('MunifTanjim/prettier.nvim') -- Prettier plugin for Neovim's built-in LSP client


  use 'lewis6991/gitsigns.nvim'
  use 'dinhhuy258/git.nvim'
  use { 'akinsho/git-conflict.nvim', tag = "*" }

  use 'akinsho/toggleterm.nvim'

  use { 'filipdutescu/renamer.nvim', requires = { 'nvim-lua/plenary.nvim' } }

  use {
    'creativenull/efmls-configs-nvim',
    tag = 'v1.*', -- tag is optional, but recommended
    requires = { 'neovim/nvim-lspconfig' },
  }

  -- Automatically set up your configuration after cloning packer.nvim
  if PACKER_BOOTSTRAP then
    require("packer").sync()
  end
end)
