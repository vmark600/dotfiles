local mason_status, mason = pcall(require, "mason")
if (not mason_status) then return end
local slspconfig_tatus, lspconfig = pcall(require, "mason-lspconfig")
if (not slspconfig_tatus) then return end

mason.setup({})

lspconfig.setup {
  automatic_installation = true
}
